//
//  ListInforCell.swift
//  DemoIOS
//
//  Created by kien on 30/07/2018.
//  Copyright © 2018 anh.buiphuong. All rights reserved.
//

import UIKit

class ListInforCell: UITableViewCell {

    @IBOutlet weak var addUser: UILabel!
    @IBOutlet weak var emailUser: UILabel!
    @IBOutlet weak var phoneUser: UILabel!
    @IBOutlet weak var nameUser: UILabel!
    @IBOutlet weak var imageUser: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
