//
//  ViewController.swift
//  DemoIOS
//
//  Created by anh.buiphuong on 7/30/18.
//  Copyright © 2018 anh.buiphuong. All rights reserved.
//

import UIKit
struct Infor{
    var name :String?
    var imageUrl : String?
    var phone: String?
    var email:String?
    var add: String?
}
class ViewController: UIViewController , UITableViewDelegate, UITableViewDataSource{

    @IBOutlet weak var listUser_Tbv: UITableView!
    var listInfor: [Infor] = []
    override func viewDidLoad() {
        super.viewDidLoad()
         print("view Did load  ")
        readInforList()
        listUser_Tbv.dataSource = self
        listUser_Tbv.delegate = self
        // Do any additional setup after loading the view, typically from a nib.
    }
    override func didRotate(from fromInterfaceOrientation: UIInterfaceOrientation){
        self.listUser_Tbv.reloadData()
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        print(listInfor.count)
        return listInfor.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! ListInforCell
        cell.nameUser.text = listInfor[indexPath.row].name
        cell.emailUser.text = listInfor[indexPath.row].email
        cell.addUser.text = listInfor[indexPath.row].add
        cell.phoneUser.text = listInfor[indexPath.row].phone
        cell.imageUser.image = nil
        cell.tag = indexPath.row
        if let url = URL(string: listInfor[indexPath.row].imageUrl!) {
            DispatchQueue.global().async {
                let data = try? Data(contentsOf: url)
                DispatchQueue.main.async {
                    if cell.tag == indexPath.row {
                        let image = UIImage(data: data!)
                        cell.imageUser.image = self.resizeImage(with: image, scaledToFill: cell.imageUser.frame.size)
                    }
                }
            }
        }
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if self.view.frame.height > self.view.frame.width {
            return self.view.frame.size.height / 5
        }else {
            return self.view.frame.height / 3
        }
        
    }
    func resizeImage(with image: UIImage?, scaledToFill size: CGSize) -> UIImage? {
        let scale: CGFloat = max(size.width / (image?.size.width ?? 0.0), size.height / (image?.size.height ?? 0.0))
        let width: CGFloat = (image?.size.width ?? 0.0) * scale
        let height: CGFloat = (image?.size.height ?? 0.0) * scale
        let imageRect = CGRect(x: (size.width - width) / 2.0, y: (size.height - height) / 2.0, width: width, height: height)
        UIGraphicsBeginImageContextWithOptions(size, false, 0)
        image?.draw(in: imageRect)
        let newImage: UIImage? = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return newImage
    }
    func readInforList(){
        var propertyListFormat =  PropertyListSerialization.PropertyListFormat.xml //Format of the Property List.
        var plistData: [String: AnyObject] = [:] //Our data
        let plistPath: String? = Bundle.main.path(forResource: "contact", ofType: "plist")! //the path of the data
        let plistXML = FileManager.default.contents(atPath: plistPath!)!
        do{
            plistData = try PropertyListSerialization.propertyList(from: plistXML, options: .mutableContainersAndLeaves, format: &propertyListFormat) as! [String:NSArray]
            for (_,value) in plistData {
                listInfor = getInforToStringArray(data: value as! NSArray)
            }
        }
        catch {
            print("Error reading plist: \(error), format: \(propertyListFormat)")
        }
    }
    func getInforToStringArray(data :NSArray) -> [Infor] {
        var listInfor : [Infor] = []
        for item in data {
            if let item = item as? NSDictionary {
                var infor = Infor()
                infor.name = item.value(forKey: "name") as? String
                infor.email = item.value(forKey: "email") as? String
                infor.phone = item.value(forKey: "phone") as? String
                infor.add = item.value(forKey: "address") as? String
                infor.imageUrl = item.value(forKey: "image") as? String
            //    print(infor.email)
                listInfor.append(infor)
            }
        }
        return listInfor
    }
    
    /// Read UserInfo Plist data and convert to string arrays contains key, value for list picker select user info
//    static func readUserInfoPList() {
//        var propertyListForamt =  PropertyListSerialization.PropertyListFormat.xml //Format of the Property List.
//        var plistData: [String: NSArray] = [:] //Our data
//        let plistPath: String? = Bundle.main.path(forResource: "UserInfo", ofType: "plist")! //the path of the data
//        let plistXML = FileManager.default.contents(atPath: plistPath!)!
//        do {
//            //convert the data to a dictionary and handle errors.
//            plistData = try PropertyListSerialization.propertyList(from: plistXML, options: .mutableContainersAndLeaves, format: &propertyListForamt) as! [String:NSArray]
//            // convert data to String arrays
//            for (key,value) in plistData {
//                if let key_ = RegisterDataPicker.textField.init(rawValue: key) {
//                    switch key_ {
//                    case .gender:
//                        convertDicArrayToStringArray(data: value, keyArray: &RegisterDataPicker.listGenderKey, valueArray: &RegisterDataPicker.listGenderString)
//                    case .age:
//                        convertDicArrayToStringArray(data: value, keyArray: &RegisterDataPicker.listAgeKey, valueArray: &RegisterDataPicker.listAgeString)
//                    case .residence:
//                        convertDicArrayToStringArray(data: value, keyArray: &RegisterDataPicker.listResidenceKey, valueArray: &RegisterDataPicker.listResidenceString)
//                    case .job:
//                        convertDicArrayToStringArray(data: value, keyArray: &RegisterDataPicker.listJobKey, valueArray: &RegisterDataPicker.listJobString)
//                    }
//                }
//            }
//        } catch {
//            print("Error reading plist: \(error), format: \(propertyListForamt)")
//        }
//    }
//
//    /// Convert data from NSDictionary Array to 2 String array key and value
//    ///
//    /// - Parameters:
//    ///   - data: NSDictionary Array data
//    ///   - keyArray: key string array return
//    ///   - valueArray: value string array return
    
    
//    static func convertDicArrayToStringArray(data: NSArray, keyArray: inout [String], valueArray: inout [String]) {
//        for item in data {
//            if let item = item as? NSDictionary, let key = item.value(forKey: "key") as? String {
//                keyArray.append(key)
//            }
//            if let item = item as? NSDictionary, let value = item.value(forKey: "value") as? String {
//                valueArray.append(value)
//            }
//        }
//    }

}

